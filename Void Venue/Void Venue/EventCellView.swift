//
//  EventCellView.swift
//  Void Venue
//
//  Created by Marc Peysale on 10/03/2016.
//  Copyright © 2016 Marc Peysale. All rights reserved.
//

import UIKit

class EventCellView: UIView {
    
    @IBOutlet weak var eventTitleLabel: UILabel!
    @IBOutlet weak var eventSubtitleLabel: UILabel!
    @IBOutlet weak var eventDateLabel: UILabel!
    
    func setUp(_ title: String, subtitle: String?, date: String) {
        self.eventTitleLabel.text = title
        self.eventSubtitleLabel.text = subtitle
        self.eventDateLabel.text = date
    }

}
