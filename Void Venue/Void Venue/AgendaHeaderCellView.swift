//
//  AgendaHeaderCellView.swift
//  Void Venue
//
//  Created by Marc Peysale on 15/04/2016.
//  Copyright © 2016 Marc Peysale. All rights reserved.
//

import UIKit

class AgendaHeaderCellView: UITableViewCell {

    @IBOutlet var monthLabel: UILabel!
    
    func setUpWithDate(_ date: Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM yyyy"
        self.monthLabel.text = dateFormatter.string(from: date)
    }

}
