//
//  VoidContactViewController.swift
//  Void Venue
//
//  Created by Marc Peysale on 15/04/2016.
//  Copyright © 2016 Marc Peysale. All rights reserved.
//

import UIKit
import MessageUI

class VoidContactViewController: UIViewController, MFMailComposeViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Infos"
        (self.navigationController as! VoidNavigationController).updateTitleForController(self)
    }

    @IBAction func sendInfosEmail(_ sender: AnyObject) {
        self.sendEmail("infos@voidvenue.com", emailTitle: "Demande d'infos - Void Venue App iOS")
    }
    
    
    @IBAction func sendProgEmail(_ sender: AnyObject) {
        self.sendEmail("prog@voidvenue.com", emailTitle: "Prog - Void Venue App iOS")
    }
    
    func sendEmail(_ emailAdress: String, emailTitle: String) {
        if !MFMailComposeViewController.canSendMail() {
            let alertController = UIAlertController(title: "Erreur", message: "Veuillez renseigner un compte e-mail dans l'app Mail de votre iPhone", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(okAction)
            present(alertController, animated: true, completion: nil)
            return
        } else {
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            
            composeVC.setToRecipients([emailAdress])
            composeVC.setSubject(emailTitle)
            composeVC.setMessageBody("\n\nEnvoyé depuis l'app Void Venue pour iOS\n", isHTML: false)
            
            composeVC.navigationBar.tintColor = UIColor.black
            
            self.present(composeVC, animated: true, completion: nil)
        }

    }
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func openFacebook(_ sender: AnyObject) {
        if UIApplication.shared.canOpenURL(URL(string: "fb://profile")!) {
            UIApplication.shared.openURL(URL(string: "fb://profile/1502739400020187")!)
        } else {
            UIApplication.shared.openURL(URL(string: "https://www.facebook.com/VoidVenue")!)
        }
    }
    
    @IBAction func openTwitter(_ sender: AnyObject) {
        if UIApplication.shared.canOpenURL(URL(string: "twitter://user")!) {
            UIApplication.shared.openURL(URL(string: "twitter://user?screen_name=VoidVenue")!)
        } else {
            UIApplication.shared.openURL(URL(string: "https://twitter.com/VoidVenue")!)
        }
    }
    
    @IBAction func openInstagram(_ sender: AnyObject) {
        if UIApplication.shared.canOpenURL(URL(string: "instagram://")!) {
            UIApplication.shared.openURL(URL(string: "instagram://user?username=voidvenue")!)
        } else {
            UIApplication.shared.openURL(URL(string: "https://www.instagram.com/voidvenue/")!)
        }
    }
    
    @IBAction func openLPDLS(_ sender: AnyObject) {
        UIApplication.shared.openURL(URL(string: "http://lpdls.com/")!)
    }
    
    
    @IBAction func openPeysaleCom(_ sender: AnyObject) {
        UIApplication.shared.openURL(URL(string: "https://marc.peysale.com")!)
    }
    
    
}
