//
//  DetailsArtistView.swift
//  Void Venue
//
//  Created by Marc 0; on 30/05/2016.
//  Copyright © 2016 Marc Peysale. All rights reserved.
//

import UIKit

class DetailsArtistView: UIView {

    @IBOutlet var bandsLabel: UILabel!
    @IBOutlet var heightConstraint: NSLayoutConstraint!
    
    var bands: String?
    
    func setUp(_ bands: String?) {
        self.bands = bands
        if bands != nil {
            self.bandsLabel.text = bands
        } else {
            self.isHidden = true
            self.heightConstraint.constant = 0
        }
    }

}
