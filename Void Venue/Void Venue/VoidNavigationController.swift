//
//  VoidNavigationController.swift
//  Void Venue
//
//  Created by Marc Peysale on 19/03/2016.
//  Copyright © 2016 Marc Peysale. All rights reserved.
//

import UIKit

class VoidNavigationController: UINavigationController {

    @IBOutlet var headerView: VoidHeaderView!
    
    var barButtonItem: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.headerView.frame.size.height)
        self.barButtonItem = UIBarButtonItem(customView: self.headerView)
    }
    
    func updateTitleForController(_ controller: UIViewController) {
        controller.navigationItem.setLeftBarButtonItems([self.barButtonItem], animated: true)
        self.headerView.setUpWithTitle(controller.title?.uppercased())
    }

}
