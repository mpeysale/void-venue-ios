//
//  EventDetailsView.swift
//  Void Venue
//
//  Created by Marc PEYSALE on 29/05/2016.
//  Copyright © 2016 Marc Peysale. All rights reserved.
//

import UIKit

class EventDetailsView: UIView {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
    @IBOutlet var headerPriceLabel: UILabel!
    @IBOutlet var eventPictureImageView: UIImageView!
    @IBOutlet var artistsView: DetailsArtistView!
    @IBOutlet var eventDetailsLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var hoursLabel: UILabel!
    @IBOutlet var imageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var dayLabel: UILabel!
    @IBOutlet var imageActivityIndicator: UIActivityIndicatorView!
    
    func setUpWithEvent(_ event: Event) {
        self.titleLabel.text = event.title
        self.subtitleLabel.text = event.subtitle
        self.headerPriceLabel.text = event.price
        self.artistsView .setUp(event.bands)
        self.dayLabel.text = event.date.dayOfMonth()
        //self.eventBand.text = event.bands
        self.priceLabel.text = event.price
        self.hoursLabel.text = ""
        if event.pictureURL != nil {
            NetworkManager.loadImage(URL(string: event.pictureURL!)!, withCompletionHandler: { (image: UIImage) in
                //self.eventPictureImageView.frame = CGRectMake(self.eventPictureImageView.frame.origin.x, self.eventPictureImageView.frame.origin.y, self.eventPictureImageView.frame.size.width, self.eventPictureImageView.frame.size.width * image.size.height / image.size.width)
                self.imageActivityIndicator.stopAnimating()
                self.eventPictureImageView.image = image
                self.imageViewHeightConstraint.constant = self.eventPictureImageView.frame.size.width * image.size.height / image.size.width
            })
        } else {
            self.imageViewHeightConstraint.constant = 0
            self.eventPictureImageView.isHidden = true
            self.imageActivityIndicator.stopAnimating()
        }
    }

}
