//
//  NetworkManager.swift
//  Void Venue
//
//  Created by Marc PEYSALE on 15/05/2016.
//  Copyright © 2016 Marc Peysale. All rights reserved.
//

import UIKit

class NetworkManager: NSObject {
    
    fileprivate static func sendAsyncRequest(_ request: URLRequest,
                                         withCompletionHandler: ((_ response: URLResponse, _ data: Data)->Void)? = nil,
                                         errorHandler: ((_ response: URLResponse, _ data: Data, _ statusCode: Int)->Void)? = nil) {
        NSURLConnection.sendAsynchronousRequest(request, queue: OperationQueue.main) { (requestResponse, requestData, requestError) -> Void in
            if requestError == nil {
                if requestData != nil {
                    if withCompletionHandler != nil {
                        withCompletionHandler!(requestResponse!, requestData!)
                    }
                }
            } else {
                print(requestError!)
                if errorHandler != nil {
                    errorHandler!(URLResponse(), Data(), -1)
                }
            }
        }
    }
    
    static func loadImage(_ imageURL: URL,
                          withCompletionHandler: ((_ image: UIImage)->Void)? = nil,
        errorHandler: ((_ response: URLResponse, _ data: Data, _ statusCode: Int)->Void)? = nil){
        NetworkManager.sendAsyncRequest(URLRequest(url: imageURL), withCompletionHandler: { (response, data) in
            if withCompletionHandler != nil {
                withCompletionHandler!(UIImage(data: data)!)
            }
        })
    }
    
    static func getEvents(_ completionHandler: ((_ events: Dictionary<Date, Array<Event>>)->Void)? = nil,
                          errorHandler: ((_ response: URLResponse, _ data: Data, _ statusCode: Int)->Void)? = nil) {
        
        let request = URLRequest(url: URL(string: "http://voidvenue.com/void_server/agenda.php")!)
        
        sendAsyncRequest(request, withCompletionHandler: { (response, data) in
            
            let eventsData = try! JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as! Dictionary<String, AnyObject>
            var events = Dictionary<Date, Array<Event>>()
            for eventMonth:(String, AnyObject) in eventsData {
                
                let monthFormatter = DateFormatter()
                monthFormatter.dateFormat = "yyyy/MM"
                
                
                var monthEvents = Array<Event>()
                
                for eventData in (eventMonth.1 as! Array<AnyObject>) {
                    let event = Event(data: eventData as! Dictionary<String, AnyObject>)
                    monthEvents.append(event)
                }
                
                events[monthFormatter.date(from: eventMonth.0)!] = monthEvents
                
            }
            
            if (completionHandler != nil) {
                completionHandler!(events)
            }
            
        }) { (response, data, statusCode) in
            
        }
        
        
    }
    
    
}
