//
//  EventDetailsViewController.swift
//  Void Venue
//
//  Created by Marc PEYSALE on 29/05/2016.
//  Copyright © 2016 Marc Peysale. All rights reserved.
//

import UIKit

class EventDetailsViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var addToCalendarButton: UIBarButtonItem!
    @IBOutlet weak var headerDayLabel: UILabel!
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var headerSubtitleLabel: UILabel!
    @IBOutlet weak var headerPriceLabel: UILabel!
    
    @IBOutlet weak var artistsView: DetailsArtistView!
    @IBOutlet weak var artistsLabel: UILabel!
    
    @IBOutlet weak var socialView: UIView!
    
    @IBOutlet weak var hoursLabel: UILabel!
    
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var eventImageViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var presaleView: UIView!
    
    @IBOutlet weak var organizersLabel: UILabel!
    @IBOutlet weak var organizersView: UIView!
    
    @IBOutlet weak var detailsWebview: UIWebView!
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var detailsWebviewHeightConstraint: NSLayoutConstraint!
    
    
    var event: Event!
    
    override func viewDidLoad() {
        setUpView()
    }
    
    
    func setUpView() {
        if self.event.date < Date() {
            self.navigationItem.rightBarButtonItem = nil
        }
        self.headerDayLabel.text = self.event.date.dayOfMonth()
        self.headerTitleLabel.text = self.event.title
        self.headerPriceLabel.text = self.event.price
        self.headerSubtitleLabel.text = self.event.subtitle
        if let bands = self.event.bands {
            self.artistsLabel.text = bands
        } else {
            self.artistsView.removeFromSuperview()
        }
        if self.event.socialLink == nil {
            self.socialView.removeFromSuperview()
        }
        
        self.priceLabel.text = self.event.price
        
        if let pictureURL = self.event.pictureURL {
            self.activityIndicator.startAnimating()
            NetworkManager.loadImage(URL(string:pictureURL)!, withCompletionHandler: { (image) in
                self.activityIndicator.stopAnimating()
                self.eventImageViewHeightConstraint.constant = (image.size.height * self.view.frame.width) / image.size.width;
                self.eventImageView.image = image
                }, errorHandler: { (response, data, statusCode) in
                    self.activityIndicator.stopAnimating()
                    self.eventImageView.removeFromSuperview()
            })
        } else {
            self.activityIndicator.stopAnimating()
            self.eventImageView.removeFromSuperview()
        }
        
        if self.event.eventBuyLink != nil {
            
        } else {
            self.presaleView.removeFromSuperview()
        }
        
        if let organizers = self.event.organizer {
            self.organizersLabel.text = organizers
        } else {
            self.organizersView.removeFromSuperview()
        }
        
        openDetailsInWebview()
        
    }
    
    func openDetailsInWebview () {
        if let detailsHTML = self.event.details {
            self.detailsWebview.delegate = self
            if let filepath = Bundle.main.path(forResource: "template", ofType: "html") {
                do {
                    let contents = try String(contentsOfFile: filepath)
                    self.detailsWebview.loadHTMLString(String.init(format: contents, detailsHTML), baseURL: nil)
                } catch {
                    self.detailsView.removeFromSuperview()
                }
            }
        } else {
            self.detailsView.removeFromSuperview()
        }
        
    }
    
    @IBAction func openSocialLink(_ sender: AnyObject) {
        if let url = self.event.socialLink {
            UIApplication.shared.openURL(URL(string: url)!)
        }
    }
    
    @IBAction func addToCalendar(_ sender: AnyObject) {
        let alertController = UIAlertController(title: "Information", message: "Ajouter un rappel pour \(self.event.title!) ?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
            self.addEventToCalendar()
        })
        
        let cancelAction = UIAlertAction(title: "Annuler", style: .cancel, handler: nil)
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func addEventToCalendar() {
        
        UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: .alert, categories: nil))
        
        let notification = UILocalNotification()
        notification.fireDate = self.event.date.dateMinus3Hours()
        notification.soundName = UILocalNotificationDefaultSoundName
        notification.alertBody = self.event.title
        notification.timeZone = NSTimeZone.default
        UIApplication.shared.scheduleLocalNotification(notification)
        
    }
    
    @IBAction func openPresaleLink(_ sender: AnyObject) {
        if let buyLink = self.event.eventBuyLink {
            UIApplication.shared.openURL(URL(string: buyLink)!)
        }
    }
    
    @IBAction func openOrganizersExternalLink(_ sender: AnyObject) {
        if let organizersLink = self.event.organizerExternalLink {
            UIApplication.shared.openURL(URL(string: organizersLink)!)
        }
    }
    
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.detailsWebviewHeightConstraint.constant = webView.scrollView.contentSize.height
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == UIWebViewNavigationType.linkClicked {
            UIApplication.shared.openURL(request.url!)
            return false
        } else {
            return true
        }
    }
    
}
