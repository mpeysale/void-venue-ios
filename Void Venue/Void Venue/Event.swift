//
//  Event.swift
//  Void Venue
//
//  Created by Marc Peysale on 10/03/2016.
//  Copyright © 2016 Marc Peysale. All rights reserved.
//

import UIKit

class Event: NSObject {
    
    init(data: Dictionary<String, AnyObject>) {
        self.title = data["event_title"] as! String
        self.subtitle = data["subtitle"] as? String
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        self.date = dateFormatter.date(from: data["event_date_start"] as! String)!
        if let dateEndString = data["event_date_end"] as? String {
            self.dateEnd = dateFormatter.date(from: dateEndString)
        }
        self.price = data["price"] as? String
        self.bands = data["bands"] as? String
        self.pictureURL = data["event_picture"] as? String
        self.organizer = data["organizer"] as? String
        self.organizerExternalLink = data["organizer_link"] as? String
        self.socialLink = data["event_social_link"] as? String
        self.eventBuyLink = data["event_buy_link"] as? String
        self.details = data["details_html"] as? String
        self.details = self.details?.replacingOccurrences(of: "\r", with: "").replacingOccurrences(of: "&amp;", with: "&").replacingOccurrences(of: "&nbsp;", with: "").replacingOccurrences(of: "\n\n", with: "<br />").replacingOccurrences(of: "\n", with: "<br />")
    }
    
    init(date: Date?, title: String, subtitle: String?) {
        if (date != nil) {
            self.date = date
        }
        self.title = title
        self.subtitle = subtitle
    }
    
    var date: Date!
    var title: String!
    var subtitle: String?
    var dateEnd: Date?
    var price: String?
    var bands: String?
    var location: String?
    var organizer: String?
    var organizerExternalLink: String?
    var socialLink: String?
    var pictureURL: String?
    var eventBuyLink: String?
    var details: String?

}
