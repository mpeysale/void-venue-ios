//
//  VoidInfosViewController.swift
//  Void Venue
//
//  Created by Marc Peysale on 15/04/2016.
//  Copyright © 2016 Marc Peysale. All rights reserved.
//

import UIKit

class VoidInfosViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Void"
        (self.navigationController as! VoidNavigationController).updateTitleForController(self)
    }
    
    

}
