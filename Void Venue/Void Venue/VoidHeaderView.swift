//
//  VoidHeaderView.swift
//  Void Venue
//
//  Created by Marc Peysale on 10/04/2016.
//  Copyright © 2016 Marc Peysale. All rights reserved.
//

import UIKit

class VoidHeaderView: UIView {

    @IBOutlet weak var titleLabel: UILabel!
    
    func setUpWithTitle(_ title: String?) {
        self.titleLabel.text = title;
    }
    
}
