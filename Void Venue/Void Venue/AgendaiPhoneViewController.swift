//
//  AgendaiPhoneViewController.swift
//  Void Venue
//
//  Created by Marc Peysale on 10/03/2016.
//  Copyright © 2016 Marc Peysale. All rights reserved.
//

import UIKit

class AgendaiPhoneViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIViewControllerPreviewingDelegate {

    @IBOutlet weak var eventsTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    var events: Dictionary<Date, Array<Event>>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.events = Dictionary<Date, Array<Event>>()
        self.title = "Agenda"
        self.eventsTableView.contentInset.bottom = CGFloat(8)
        (self.navigationController as! VoidNavigationController).updateTitleForController(self)
        
        NetworkManager.getEvents({ (events) in
            self.activityIndicator.stopAnimating()
            self.events = events
            self.eventsTableView.reloadData()
            }) { (response, data, statusCode) in
                self.activityIndicator.stopAnimating()
        }
        
        
        registerForPreviewing(with: self, sourceView: self.eventsTableView)
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventCell") as! EventCell
        
        let event = self.events[Array(self.events.keys)[(indexPath as NSIndexPath).section]]![(indexPath as NSIndexPath).row]
        cell.cellView .setUp(event.title, subtitle: event.subtitle, date: event.date != nil ? event.date.dayOfMonth() : "")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events[Array(events.keys)[section]]!.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.events.keys.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! AgendaHeaderCellView
        header.setUpWithDate(Array(self.events.keys)[section])
        return header;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {        
        self.performSegue(withIdentifier: "eventDetailsSegue", sender: self.events[Array(self.events.keys)[(indexPath as NSIndexPath).section]]![(indexPath as NSIndexPath).row])
    }
    
    func viewControllerForIndexPath(indexPath: IndexPath) -> UIViewController {
        if let controller = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "eventDetailsViewController") as? EventDetailsViewController {
            let event = self.events[Array(self.events.keys)[(indexPath as NSIndexPath).section]]![(indexPath as NSIndexPath).row]
            controller.event = event
            return controller
        }
        
        return UIViewController()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "eventDetailsSegue") {
            let detailsController = segue.destination as! EventDetailsViewController
            detailsController.event = sender as! Event
        }
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        if let indexPath = self.eventsTableView.indexPathForRow(at: location) {
            previewingContext.sourceRect = self.eventsTableView.rectForRow(at: indexPath)
            return viewControllerForIndexPath(indexPath: indexPath)
        }
        return nil
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        self.navigationController?.pushViewController(viewControllerToCommit, animated: true)
    }
    
}
