//
//  NSDate+Extension.swift
//  Void Venue
//
//  Created by Marc Peysale on 19/03/2016.
//  Copyright © 2016 Marc Peysale. All rights reserved.
//

import Foundation

extension Date {
    func dayOfMonth () -> String {
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        let components = (cal as NSCalendar).components([.day], from: self)
        return "\(components.day!)"
    }
    
    func dateMinus3Hours() -> Date {
        let calendar = Calendar.current
        return calendar.date(byAdding: .hour, value: -3, to: self)!
    }
}
